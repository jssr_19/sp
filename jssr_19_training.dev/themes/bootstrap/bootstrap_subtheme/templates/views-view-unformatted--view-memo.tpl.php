<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?> style="border:1px solid gray; text-align:center; border-radius:4px; box-shadow:1px 1px 1px 1px black inset;">
    <?php print $row; ?>
  </div><br>
<?php endforeach; ?>
