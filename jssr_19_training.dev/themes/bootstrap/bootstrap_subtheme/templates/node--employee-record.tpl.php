<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div style="background-color:white" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h3<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print "<table style=' text-align:center;'><tr><td> <img src='" . file_create_url($node->field_picture[LANGUAGE_NONE][0]['uri']) . "' align='left' class='emp-print-image'/></td><td style='vertical-align:bottom'>" . render($content['field_first_name']) . "</td><td style='vertical-align:bottom'>" . render($content['field_middle_name']) . "</td><td style='vertical-align:bottom'>" . render($content['field_last_name']) . "</td></tr></table>"; ?></a></h3>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?> style="background-color:white; color:black">
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_last_name']);
	 // $content['field_last_name']['#label_display']='hidden';
	  hide($content['field_first_name']);
	  hide($content['field_middle_name']);
	  hide($content['field_suffix']);
	  hide($content['field_birth_date']);
	  hide($content['field_address']);
	  hide($content['field_sex']);
	  hide($content['field_civil_status1']);
	   hide($content['field_nationality']);
	  hide($content['field_religion']);
	  hide($content['field_contact_information']);
	  hide($content['field_email']);
	  hide($content['field_tax_identification_number']);
	  hide($content['field_sss_number']);
	  hide($content['field_tertiary']);
	  hide($content['field_secondary']);
	  hide($content['field_primary']);
	  hide($content['field_year_graduated_ter']);
	  hide($content['field_year_graduated_sec']);
	  hide($content['field_year_graduated_pri']);
	  hide($content['field_work_pos']);
	  hide($content['field_name_company']);
	  hide($content['field_year_work']);
	  hide($content['field_contact']);
	 // hide($content['field_picture']);
	 // print render($content) . "<br><br>";
	 print "<div><h3>Personal Information</h3><hr><table class='width-table'>";		
	 print "<tr>";
			print "<td>Birth Date:</td><td>";
			print "<br>" . render($content['field_birth_date']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Sex:</td><td>";
			print render($content['field_sex']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Civil Status:</td><td>";
			print render($content['field_civil_status1']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Nationality:</td><td>";
			print render($content['field_nationality']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Religion:</td><td>";
			print render($content['field_religion']);
			print "</td>";		
	print "</tr><tr>";
			print "<td>Contact Number:</td><td>";
			print render($content['field_contact_information']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Address:</td><td>";
			print render($content['field_address']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Email Address:</td><td>";
			print render($content['field_email']);
			print "</td>";
	print "</tr><tr>";
			print "<td>TIN Number:</td><td>";
			print render($content['field_tax_identification_number']);
			print "</td>";
	print "</tr><tr>";
			print "<td>SSS Number:</td><td>";
			print render($content['field_sss_number']);
			print "</td>";
	print "</tr><tr>";
			print "<td>Contact Person:</td><td>";
			print render($content['field_contact']);
			print "</td>";
			print "</tr>";			
	print "</table>";
	print "<h3>Educational Background</h3><hr>";
	print "<table class='width-table'>";
		print "<tr text-align:center;>";
			print "<td>";
				print "Level ";
			print "</td>";
			print "<td>";
				print "Year ";
			print "</td>";
			print "<td>";
				print "School ";
			print "</td>";
		print"</tr>";
		print "<tr>";
			print "<td class='td-bold'>";
				print "Tertiary";
			print "</td>";
			print "<td>";
				print render($content['field_year_graduated_ter']);
			print "</td>";
			print "<td>";
				print render($content['field_tertiary']);
			print "</td>";
		print "</tr>";
		print "<tr>";
			print "<td class='td-bold'>";
				print "Secondary";
			print "</td>";
			print "<td>";
				print render($content['field_year_graduated_sec']);
			print "</td>";
			print "<td>";
				print render($content['field_secondary']);
			print "</td>";
		print "</tr>";
		print "<tr>";
			print "<td class='td-bold'>";
				print "Primary";
			print "</td>";
			print "<td>";
				print render($content['field_year_graduated_pri']);
			print "</td>";
			print "<td>";
				print render($content['field_primary']);
			print "</td>";
		print "</tr>";
	print "</table>";
	print "<h3>Work History</h3><hr>";
	print "<table class='width-table'>";
		print "<tr text-align:center;>";
			print "<td class='td-bold'>";
				print "<span class='td-not-bold'>Position</span>";
				print render($content['field_work_pos']);
			print "</td>";
			print "<td class='td-bold'>";
				print "<span class='td-not-bold'>Company</span>";
				print render($content['field_name_company']);
			print "</td>";
			print "<td class='td-bold'>";
				print "<span class='td-not-bold'>Year</span>";
				print render($content['field_year_work']);
			print "</td>";
		print "</tr>";
	print "</table>";	
	 print "</div><br>";
	// print "<img src='sites/default/files/" . render($content['field_picture']) . "'/>" ;
    ?>
	
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</div>
<style>
	div.field-label{
		font-weight:light;
		color:gray;
		display:none;
	}
	div.field-item.even
	{
		font-weight:bold;
	}
	
	hr
	{
		border:1px solid black;
		height:2px;
		background-color:black;
	}
	.td-bold
	{
		font-weight:bold;
	}
	.td-not-bold
	{
		font-weight:normal;
	}
	.width-table
	{
		width:95%;
	}
	.emp-print-image
	{
		width:218px;
		height:218px;
		border:2px solid black;
	}
	
</style>