<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?> style="border:1px solid gray; width:70%; margin-left:auto; margin-right:auto; text-align:center; border-radius:5px; box-shadow:3px 3px 3px rgb(45,45,45) inset; -moz-box-shadow:3px 3px 3px rgb(45,45,45) inset; -webkit-box-shadow:3px 3px 3px rgb(45,45,45) inset; ">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>