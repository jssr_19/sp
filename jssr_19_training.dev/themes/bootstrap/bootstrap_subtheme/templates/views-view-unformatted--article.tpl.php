<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3 style="color:dodgerblue"><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?> style="border:1px solid gray; border-radius:4px; box-shadow:1px 1px 15px dodgerblue inset; background-color:white;" align="center">
    <?php print $row; ?>
  </div><br>
<?php endforeach; ?>